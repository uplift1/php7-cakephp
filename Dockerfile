FROM omneedia/dev-php7-cakephp

COPY app/ /var/www/
RUN chmod 777 -Rf /var/www

RUN composer install --no-scripts
